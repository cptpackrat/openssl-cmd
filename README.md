@nfi/openssl-cmd
======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

Basic wrapper for the OpenSSL command-line utility.

## Installation
```
npm install @nfi/openssl-cmd
```

## Usage Example
```js
const { openssl } = require('@nfi/openssl-cmd')

/**
 * Generate a new RSA private key.
 * @param {number} [bits] Optional key size; default 2048.
 * @param {string} [pass] Optional passphrase; unencrypted by default.
 * @returns {Promise<Buffer>} PEM-formatted private key.
*/
async function genpkey(bits, pass) {
  return (await openssl('genpkey')
    .args('-algorithm', 'rsa')
    .argsIf(bits, '-pkeyopt', `rsa_keygen_bits:${bits}`)
    .argsIf(pass, '-pass', `pass:${pass}`, '-aes256')
    .exec()
  ).stdout
}
```

[npm-image]: https://img.shields.io/npm/v/@nfi/openssl-cmd.svg
[npm-url]: https://www.npmjs.com/package/@nfi/openssl-cmd
[pipeline-image]: https://gitlab.com/cptpackrat/openssl-cmd/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/openssl-cmd/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/openssl-cmd/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/openssl-cmd/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
