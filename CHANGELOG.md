# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.2](https://gitlab.com/cptpackrat/openssl-cmd/compare/v2.0.0...v2.0.2) (2020-03-29)


### Housekeeping

* Switched linter to standardx for better TS support. ([6a52a24](https://gitlab.com/cptpackrat/openssl-cmd/commit/6a52a24322fa6edc5d24db23d13ed52907bb5e22))
* Updated typescript target to ES2018. ([a7914e3](https://gitlab.com/cptpackrat/openssl-cmd/commit/a7914e34b5761e98f85f3e27be06c90168b41ef1))
* Updated dependencies. ([6e053e4](https://gitlab.com/cptpackrat/openssl-cmd/commit/6e053e4dc9cffa9b9f86f0b4120bb7f71474fa87))
* Various CI changes. ([30f11b9](https://gitlab.com/cptpackrat/openssl-cmd/commit/30f11b921b666471dc42bdea55c4337ce32870ca))q

## [2.0.1](https://gitlab.com/cptpackrat/openssl-cmd/compare/v2.0.0...v2.0.1) (2020-01-02)


### Housekeeping

* Updated dependencies. ([0cb9e93](https://gitlab.com/cptpackrat/openssl-cmd/commit/0cb9e93bc054e7c77778ec9ba0f47a7299bb7e5b))

## [2.0.0](https://gitlab.com/cptpackrat/openssl-cmd/compare/v1.0.0...v2.0.0) (2019-11-10)


### BREAKING CHANGES

* Contents of stdout/stderr are now captured and returned as buffers.

### Bug Fixes

* Fixed mishandling of binary data in stdout/stderr. ([46af7cf](https://gitlab.com/cptpackrat/openssl-cmd/commit/46af7cff78a8965c0866b47fd7fe6c43c7400b67))
