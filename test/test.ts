'use strict'

import { expect } from 'chai'
import {
  command,
  OpensslCommand,
  OpensslError,  /* eslint-disable-line */
  OPENSSL_BIN
} from '../src'

describe('openssl', () => {
  describe('command', () => {
    it('returns a new OpensslCommand', () => {
      expect(command('foo')).to.be.instanceof(OpensslCommand)
    })
  })
  describe('OpensslCommand', () => {
    it('executes commands', async () => {
      const res = await command('version').exec()
      expect(res.stdout.toString()).to.match(/^OpenSSL \d+\.\d+\.\d+[a-z]?/)
    })
    it('captures stdout and stderr', async () => {
      const res = await command('genrsa').exec()
      expect(res.stdout.toString()).to.contain('-----BEGIN RSA PRIVATE KEY-----')
      expect(res.stderr.toString()).to.contain('Generating RSA private key')
    })
    it('captures binary output correctly', async () => {
      const res = await command('rand').args('500000').exec()
      expect(res.stdout.length).to.equal(500000)
    })
    it('accepts arguments', async () => {
      const res = await command('passwd')
        .args('-crypt')
        .args('-salt', 'foo')
        .args('bar')
        .exec()
      expect(res.stdout.toString().trim()).to.equal('foXrpAKGo3142')
    })
    it('accepts conditional arguments', async () => {
      async function test (cond: boolean) {
        return (await command('passwd')
          .args('-crypt', '-salt', 'foo')
          .argsIf(cond, 'bar')
          .argsIf(!cond, 'boo')
          .exec()
        ).stdout.toString().trim()
      }
      expect(await test(true)).to.equal('foXrpAKGo3142')
      expect(await test(false)).to.equal('foOpjWmaay/JE')
    })
    it('accepts stdin', async () => {
      const res = await command('passwd')
        .args('-stdin', '-crypt', '-salt', 'foo')
        .stdin('bar')
        .exec()
      expect(res.stdout.toString().trim()).to.equal('foXrpAKGo3142')
    })
    it('accepts conditional stdin', async () => {
      async function test (cond: boolean) {
        return (await command('passwd')
          .args('-stdin', '-crypt', '-salt', 'foo')
          .stdinIf(cond, 'bar')
          .stdinIf(!cond, 'boo')
          .exec()
        ).stdout.toString().trim()
      }
      expect(await test(true)).to.equal('foXrpAKGo3142')
      expect(await test(false)).to.equal('foOpjWmaay/JE')
    })
  })
  describe('OpensslError', () => {
    let err: OpensslError
    before(async () => {
      try {
        await command('nope').exec()
      } catch (e) {
        err = e
      }
    })
    it('captures openssl path', () => {
      expect(err.bin).to.equal(OPENSSL_BIN)
    })
    it('captures subcommand name', () => {
      expect(err.cmd).to.equal('nope')
    })
    it('captures exit code', () => {
      expect(err.code).to.equal(1)
    })
    it('captures stdout', () => {
      expect(err.stdout.toString()).to.equal('')
    })
    it('captures stderr', () => {
      expect(err.stderr.toString()).to.contain('Invalid command \'nope\'')
    })
  })
})
