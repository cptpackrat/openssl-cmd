
import { spawn } from 'child_process'

export const OPENSSL_BIN = '/usr/bin/openssl'

/** Return a new command object.
  * @param cmd Subcommand to execute. */
export function command (cmd: string) {
  return new OpensslCommand(cmd)
}

/** Alias for command; returns a new command object.
  * @param cmd Subcommand to execute. */
export const openssl = command

/** Wrapper for the openssl commandline tool. */
export class OpensslCommand {
  private _cmd: string
  private _args: string[] = []
  private _stdin: any[] = []

  /** @param cmd Subcommand to execute. */
  constructor (cmd: string) {
    this._cmd = cmd
  }

  /** Append arguments to command. */
  args (...args: string[]) {
    this._args.push(...args)
    return this
  }

  /** Conditionally append arguments to a command. */
  argsIf (cond: any, ...args: string[]) {
    if (cond) {
      this._args.push(...args)
    }
    return this
  }

  /** Append chunks of data to be written to stdin. */
  stdin (...data: any[]) {
    this._stdin.push(...data)
    return this
  }

  /** Conditionally append chunks of data to be written to stdin. */
  stdinIf (cond: any, ...data: any[]) {
    if (cond) {
      this._stdin.push(...data)
    }
    return this
  }

  /** Execute command with all supplied arguments and data. */
  exec (): Promise<{
    stdout: Buffer
    stderr: Buffer
  }> {
    return new Promise((resolve, reject) => {
      const out: Buffer[] = []
      const err: Buffer[] = []
      const sub = spawn(OPENSSL_BIN, [this._cmd].concat(this._args), { shell: false })
      sub.stdout.on('data', (data) => out.push(data))
      sub.stderr.on('data', (data) => err.push(data))
      sub.on('error', (err) => reject(err))
      sub.on('close', (code) => {
        return code
          ? reject(new OpensslError(this._cmd, code, Buffer.concat(out), Buffer.concat(err)))
          : resolve({
            stdout: Buffer.concat(out),
            stderr: Buffer.concat(err)
          })
      })
      if (this._stdin.length > 0) {
        for (const data of this._stdin) {
          sub.stdin.write(data)
        }
        sub.stdin.end()
      }
    })
  }
}

/** Custom error for openssl command failures. */
export class OpensslError extends Error {
  /** Full path to openssl binary. */
  readonly bin: string
  /** Subcommand that was executed. */
  readonly cmd: string
  /** Exit code. */
  readonly code: number
  /** Output captured from stdout. */
  readonly stdout: Buffer
  /** Output captured from stderr. */
  readonly stderr: Buffer

  /** @param cmd Subcommand that was executed.
    * @param code Exit code.
    * @param stdout Output captured from stdout.
    * @param stderr Output captured from stderr. */
  constructor (cmd: string, code: number, stdout: Buffer, stderr: Buffer) {
    super(`openssl command '${cmd}' failed with code ${code}`)
    this.bin = OPENSSL_BIN
    this.cmd = cmd
    this.code = code
    this.stdout = stdout
    this.stderr = stderr
  }
}
